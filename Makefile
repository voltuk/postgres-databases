help:	## Show this help.
	@fgrep -h "##" $(MAKEFILE_LIST) | fgrep -v fgrep | sed -e 's/\\$$//' | sed -e 's/##//'

run:	## run containers
	docker-compose -f stack.yml up &

stop:	## stop containers
	docker-compose -f stack.yml down

dbuser=postgres
dbname=voltuk

sql_prompt:	## connect to database
	pgcli --username $(dbuser) --dbname $(dbname) --port 5432 --host localhost --password
