\connect "voltuk";

DROP TABLE IF EXISTS "wards";
CREATE TABLE "public"."wards" (
    "id" character varying(15) NOT NULL,
    "name" character varying(50) NOT NULL,
    "district_id" character varying(15) NOT NULL
) WITH (oids = false);

DROP TABLE IF EXISTS "districts";
CREATE TABLE "public"."districts" (
    "id" character varying(15) NOT NULL,
    "name" character varying(50) NOT NULL,
    "county_id" character varying(15) NOT NULL
) WITH (oids = false);

DROP TABLE IF EXISTS "counties";
CREATE TABLE "public"."counties" (
    "id" character varying(15) NOT NULL,
    "name" character varying(50) NOT NULL,
    "region_id" character varying(15) NOT NULL
) WITH (oids = false);

DROP TABLE IF EXISTS "regions";
CREATE TABLE "public"."regions" (
    "id" character varying(15) NOT NULL,
    "name" character varying(50) NOT NULL,
    "country_id" character varying(15) NOT NULL
) WITH (oids = false);

DROP TABLE IF EXISTS "countries";
CREATE TABLE "public"."countries" (
    "id" character varying(10) NOT NULL,
    "name" character varying(20) NOT NULL
) WITH (oids = false);
